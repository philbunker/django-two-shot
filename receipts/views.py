from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ExpenseCategoryForm, ReceiptForm, AccountForm
from django.contrib.auth.decorators import login_required


#localhost:8000/receipts/
@login_required
def list_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipts': receipts,
    }
    return render(request, "receipts/list_receipts.html", context)

#localhost:8000/receipts/create/
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_receipt.html", context)

#localhost:8000/receipts/categories/
@login_required
def list_categories(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'categories': categories,
    }
    return render(request, "receipts/list_categories.html", context)

#localhost:8000/receipts/accounts/
@login_required
def list_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        'accounts': accounts,
    }
    return render(request, "receipts/list_accounts.html", context)

#localhost:8000/receipts/categories/create/
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)

#localhost:8000/receipts/accounts/create/
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
